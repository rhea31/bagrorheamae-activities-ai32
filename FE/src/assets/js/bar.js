import { Bar } from 'vue-chartjs' 

export default {
  extends: Bar,
  mounted () {
    this.renderChart({
      labels: ['BORROWED', 'RETURNED'],
      datasets: [
        {
          label: 'Borrowed & Returned',
          backgroundColor:'#810C0C',
          data: [ 19,13,0],
          borderColor: '#9DBCC7',
          borderWidth: 2,
        }
      ],
    }, {responsive: true, maintainAspectRatio: false})
  }
}
