import { Pie } from 'vue-chartjs'

export default {
  extends: Pie,
  mounted () {
    this.renderChart({
      labels: ['Popular books','Copies Available'],
      datasets: [
        {
          backgroundColor: ['#810C0C','#CCB6B6'],
          borderColor: '#9DBCC7',
          borderWidth: 1,
          data: [10,16],
        }
      ]
    }, {responsive: true, maintainAspectRatio: false})
  },
}
