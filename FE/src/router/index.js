import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../components/dashboard'
import Book from '../components/book-management'
import Patron from '../components/patron-management'
import Settings from '../components/settings'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/Book',
    name: 'Book',
    component: Book
  },
  {
    path: '/Patron',
    name: 'Patron',
    component: Patron
  },
  {
    path: '/Settings',
    name: 'Settings',
    component: Settings
  }
 
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
