import Vue from 'vue'
import Vuex from 'vuex'
import patron from './modules/patron'
import book from './modules/book'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    patron,
    book
  }
})
