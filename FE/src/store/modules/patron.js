import axios from 'axios'

const patron = {
    namespaced:true,
    state:() => ({
        
        Data:[]

    }),
    mutations:{

        GET_DATA(state,payload)
        {
            state.Data = payload
        }

    },
    actions:{

        getData({commit})
        {
            axios.get('http://127.0.0.1:8000/patrons')
            .then(res => { 
                commit('GET_DATA' , res.data)
            })
            .catch(error => {
                console.log(error);
            })
        },

        addData({dispatch},data)
        {
            axios.post('http://127.0.0.1:8000/patrons', data )
            .then( () => { 
                dispatch('getData')
            })
            .catch(error => {
                console.log(error);
            })
        },

        updateData({dispatch},data)
        {
            axios.put(`http://127.0.0.1:8000/patrons/${data.id}`, data )
            .then( () => { 
                dispatch('getData')
            })
            .catch(error => {
                console.log(error);
            })
        },

        deleteData({dispatch},data)
        {
            axios.delete(`http://127.0.0.1:8000/patrons/${data.id}`, data )
            .then( () => { 
                dispatch('getData')
            })
            .catch(error => {
                console.log(error);
            })
        }

    },
    getters:{

        data(state)
        {
            return state.Data
        }

    }

}

export default patron