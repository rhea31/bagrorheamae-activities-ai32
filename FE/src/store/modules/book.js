import axios from 'axios'

const book = {
    namespaced:true,
    state:() => ({
        
        Data:[],
        categories:[]

    }),
    mutations:{

        GET_DATA(state,payload)
        {
            state.Data = payload
        },
        
        GET_CATEGORIES(state,payload)
        {
            state.categories = payload
        }

    },
    actions:{

        getCategory({commit})
        {
            axios.get('http://127.0.0.1:8000/categories')
            .then(res => { 
                commit('GET_CATEGORIES' , res.data)
            })
            .catch(error => {
                console.log(error);
            })
        },

        getData({commit})
        {
            axios.get('http://127.0.0.1:8000/books')
            .then(res => { 
                commit('GET_DATA' , res.data)
            })
            .catch(error => {
                console.log(error);
            })
        },

        addData({dispatch},data)
        {
            axios.post('http://127.0.0.1:8000/books', data )
            .then( () => { 
                dispatch('getData')
            })
            .catch(error => {
                console.log(error);
            })
        },

        updateData({dispatch},data)
        {
            axios.put(`http://127.0.0.1:8000/books/${data.id}`, data )
            .then( () => { 
                dispatch('getData')
            })
            .catch(error => {
                console.log(error);
            })
        },

        deleteData({dispatch},data)
        {
            axios.delete(`http://127.0.0.1:8000/books/${data.id}`, data )
            .then( () => { 
                dispatch('getData')
            })
            .catch(error => {
                console.log(error);
            })
        }

    },
    getters:{

        data(state)
        {
            return state.Data
        },
        
        category(state)
        {
            return state.categories
        }

    }

}

export default book