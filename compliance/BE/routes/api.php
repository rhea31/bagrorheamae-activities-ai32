<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BusDriverController;
use App\Http\Controllers\DestinationController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\BusesController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resources([
    'bus-driver' => BusDriverController::class,
    'destination' => DestinationController::class,
    'ticket' => TicketController::class,
    'buses' => BusesController::class
]);

