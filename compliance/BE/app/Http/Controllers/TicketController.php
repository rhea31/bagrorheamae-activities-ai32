<?php

namespace App\Http\Controllers;
use App\Models\Ticket;
use App\Http\Requests\TicketRequest;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::all();

        return response()->json($tickets);
    }

    /**
     * Store a newly created resource in storage.
       * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketRequest $request)
    {   
        $validated = $request->validated();

        $tickets = new Ticket;

        $tickets->bus_id = $request->input('bus_id');
        $tickets->ticket_number = $request->input('ticket_number');

        $tickets->save();

        return response()->json($tickets);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TicketRequest $request, $id)
    {   
        $validated = $request->validated();
        
        $tickets = Ticket::find($id);

        $tickets->bus_id = $request->input('bus_id');
        $tickets->ticket_number = $request->input('ticket_number');

        $tickets->save();

        return response()->json($tickets);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tickets = Ticket::find($id);

        $tickets->delete();

        return response()->json($tickets);
    }
}
