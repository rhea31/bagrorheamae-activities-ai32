<?php

namespace App\Http\Controllers;
use App\Models\Destination;
use Illuminate\Http\Request;
use App\Http\Requests\DestinationRequest;

class DestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $destination = Destination::all();

        return response()->json($destination);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DestinationRequest $request)
    {
        $validated = $request->validated();

        $destinations = new Destination;

        $destinations->destination = $request->input('destination');
        $destinations->cost = $request->input('cost');

        $destinations->save();

        return response()->json($destinations);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DestinationRequest $request, $id)
    {
        $validated = $request->validated();
        
        $destinations = Destination::find($id);

        $destinations->destination = $request->input('destination');
        $destinations->cost = $request->input('cost');

        $destinations->save();

        return response()->json($destinations);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destinations = Destination::find($id);
        
        $destinations->delete();
        
        return response()->json($destinations);

    }
}
