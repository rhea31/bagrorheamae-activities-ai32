<?php

namespace App\Http\Controllers;
use App\Models\BusDriver;
use Illuminate\Http\Request;
use App\Http\Requests\BusDriverRequest;

class BusDriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $busDriver = BusDriver::all();

       return response()->json($busDriver);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BusDriverRequest $request)
    {   
        $validated = $request->validated();

        $busDriver = new BusDriver;

        $busDriver->first_name = $request->input('first_name');
        $busDriver->middle_name = $request->input('middle_name');
        $busDriver->last_name = $request->input('last_name');
        $busDriver->contact = $request->input('contact');

        $busDriver->save();

        return response()->json($busDriver);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BusDriverRequest $request, $id)
    {
        $validated = $request->validated();

        $busDriver = BusDriver::find($id);

        $busDriver->first_name = $request->input('first_name');
        $busDriver->middle_name = $request->input('middle_name');
        $busDriver->last_name = $request->input('last_name');
        $busDriver->contact = $request->input('contact');

        $busDriver->save();

        return response()->json($busDriver);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $busDriver = BusDriver::find($id);

        $busDriver->delete();

        return response()->json($busDriver);
    }
}
