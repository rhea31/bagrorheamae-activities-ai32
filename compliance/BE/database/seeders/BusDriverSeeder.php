<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BusDriverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bus_drivers')->insert([
            'first_name' => 'Rhea Mae',
            'middle_name' => 'Mitra', 
            'last_name' => 'Bagro',
            'contact' => '09876543211'
        ]);
    }
}
