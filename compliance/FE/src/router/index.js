import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../components/Dashboard'
import Ticket from '../components/Ticket'
import Destination from '../components/Destination'
import BusDriver from '../components/BusDriver'
import User from '../components/User'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/User',
    name: 'User',
    component: User
  },
  {
    path: '/BusDriver',
    name: 'BusDriver',
    component: BusDriver
  },
  {
    path: '/Ticket',
    name: 'Ticket',
    component: Ticket
  },
  {
    path: '/Destination',
    name: 'Destination',
    component: Destination
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
