import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'md',
    },
    theme: {
        themes: {
          light: {
            primary: '#5E35B1',
            secondary: '#424242',
            accent: '#8c9eff',
            error: '#b71c1c',
          },
        },
    },
});
