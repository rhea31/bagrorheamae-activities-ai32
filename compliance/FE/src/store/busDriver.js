import axios from 'axios'

export default {
    namespaced: true,
    state: () => ({
        busDriverData:[],
        checkData: null,
        errorsData:[]
    }),
    mutations:{
        busDriver(state,data){
            state.busDriverData = data 
        },
        checking(state,data){
            state.checkData = data
        },
        errors(state,data){
            state.errorsData = data
        }
    },
    actions:{
        getData({commit}){
            axios.get('http://127.0.0.1:8000/api/bus-driver')
            .then(res => {
                commit('busDriver', res.data)
                console.log(res.data)
            })
        },
        postData({dispatch,commit}, data){
            axios.post('http://127.0.0.1:8000/api/bus-driver', data)
            .then(res => {
                dispatch('getData')
                console.log(res.data)
                commit('checking', true)
             })
            .catch(error => {
                console.log(error.response.data.errors)
                commit('errors', error.response.data.errors)
                commit('checking', false)
             });  
        },
        updateData({dispatch,commit}, data){
            axios.put(`http://127.0.0.1:8000/api/bus-driver/${data.id}`, data)
            .then(res => {
                dispatch('getData')
                console.log(res.data)
                commit('checking', true)
             })
            .catch(error => {
                console.log(error.response.data.errors)
                commit('errors', error.response.data.errors)
                commit('checking', false)
             }); 
        },
        deleteData({dispatch,commit},data){
            axios.delete(`http://127.0.0.1:8000/api/bus-driver/${data.id}`)
            .then(res => {
                dispatch('getData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error.response.data.errors)
                commit('errors', error.response.data.errors)
             }); 
        }
    },
    getters:{
        busDriver(state){
            return state.busDriverData
        },

        errors(state){
            return state.errorsData
        },

        checking(state){
            return state.checkData
        }
    }
}