import axios from 'axios'

export default {
    namespaced: true,
    state: () => ({
        destinationData:[],
        checkData: null,
        errorsData:[]
    }),
    mutations:{
        destination(state,data){
            state.destinationData = data 
        },
        checking(state,data){
            state.checkData = data
        },
        errors(state,data){
            state.errorsData = data
        }
    },
    actions:{
        getData({commit}){
            axios.get('http://127.0.0.1:8000/api/destination')
            .then(res => {
                commit('destination', res.data)
                console.log(res.data)
            })
        },
        postData({dispatch,commit}, data){
            axios.post('http://127.0.0.1:8000/api/destination', data)
            .then(res => {
                dispatch('getData')
                console.log(res.data)
                commit('checking', true)
             })
            .catch(error => {
                console.log(error.response.data.errors)
                commit('errors', error.response.data.errors)
                commit('checking', false)
             });  
        },
        updateData({dispatch,commit}, data){
            axios.put(`http://127.0.0.1:8000/api/destination/${data.id}`, data)
            .then(res => {
                dispatch('getData')
                console.log(res.data)
                commit('checking', true)
             })
            .catch(error => {
                console.log(error.response.data.errors)
                commit('errors', error.response.data.errors)
                commit('checking', false)
             }); 
        },
        deleteData({dispatch,commit},data){
            axios.delete(`http://127.0.0.1:8000/api/destination/${data.id}`)
            .then(res => {
                dispatch('getData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error.response.data.errors)
                commit('errors', error.response.data.errors)
             }); 
        }
    },
    getters:{
        destination(state){
            return state.destinationData
        },

        errors(state){
            return state.errorsData
        },

        checking(state){
            return state.checkData
        }
    }
}