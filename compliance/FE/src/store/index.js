import Vue from 'vue'
import Vuex from 'vuex'
import busDriver from '../store/busDriver'
import destination from '../store/destination'
import ticket from '../store/ticket'

Vue.use(Vuex)

export default new Vuex.Store({
  modules:{
    busDriver,
    destination,
    ticket
  }
})