<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CopiesValidation;
use App\Models\ReturnedBook;
use App\Models\BorrowedBook;

class ReturnedBooksController extends Controller
{
    public function store(CopiesValidation $request)
    {
        $borrowed_book = BorrowedBook::where('copies')->get();
        
        $returned_book = new ReturnedBook;
        $returned_book->copies = $request->input('copies');

        if(intval($returned_book->copies) > $borrowed){

            return abort(404);
        }

        else{

            $returned_copies = intval($return->copies) + intval($borrowed);
            $returned_book->copies = $returned_copies;
            $returned_book->save();
            return response()->json($returned_book); 
             
        }
    }
}
