<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Patron;
use App\Http\Requests\PatronValidation;

class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patrons = Patron::all();

        return response()->json($patrons);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatronValidation $request)
    {
        $patrons = new Patron;

        $patrons->last_name = $request->input('last_name');
        $patrons->first_name = $request->input('first_name');
        $patrons->middle_name = $request->input('middle_name');
        $patrons->email = $request->input('email');

        $patrons->save();

        return response()->json($patrons);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatronValidation $request, $id)
    {
        $patrons = Patron::find($id);

        $patrons->last_name = $request->input('last_name');
        $patrons->first_name = $request->input('first_name');
        $patrons->middle_name = $request->input('middle_name');
        $patrons->email = $request->input('email');

        $patrons->save();

        return response()->json($patrons);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patrons = Patron::find($id);
        
        $patrons->forceDelete();

        return response()->json($patrons);
    }
}
