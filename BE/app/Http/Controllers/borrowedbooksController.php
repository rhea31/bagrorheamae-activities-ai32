<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CopiesValidation;
use App\Models\BorrowedBook;
use App\Models\Book;

class BorrowedBooksController extends Controller
{
    public function store(CopiesValidation $request)
    {   

        $books = Book::where('copies')->get();
        
        $borrowed_book = new BorrowedBook;
        $borrowed_book->copies = $request->input('copies');

        if(intval($borrowed_book->copies) > $book){

            return abort(404);
        }

        else{

            $borrowed_book->save();
            return response()->json($borrowed_book); 
             
        }

       
    }
}
