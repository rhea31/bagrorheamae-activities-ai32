<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory; 
    public function category(){
        return $this->hasOne(Category::class);
    }

    protected $table = 'books';
    protected $fillable = ['name','author','copies'];

    public function categories()
    {
        return $this->belongsTo(Category::class);
    }
}
