<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BorrowedBook extends Model
{
    use HasFactory;
    public function borrowedbooks(){
        return $this->belongsToMany(Book::class, Patron::class);
    }

    protected $table = 'borrowed_books';
    protected $fillable = ['copies'];
    
}