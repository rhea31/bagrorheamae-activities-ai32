<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnedBook extends Model
{
    use HasFactory;
    public function returnedbooks(){
        return $this->belongsToMany(Book::class, Patron::class);
    }

    protected $table = 'returned_books';
    protected $fillable = ['copies'];

}