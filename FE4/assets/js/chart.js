var ctx = document.getElementById('bar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['BORROWED', 'RETURNED'],
        datasets: [{
            label:'Borrowed & Returned',
            backgroundColor:'#810C0C',
            borderColor: '#9DBCC7',
            borderWidth: 2,
            data: [19,13],
            
        }]
    },
    options: {
        scales: {  
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});




var ctx = document.getElementById('pie').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['Popular books','Copies Available'],
        datasets: [{
            backgroundColor:['#810C0C','#CCB6B6' ],
            borderColor: '#9DBCC7',
            borderWidth: 1,
            padding:5,
            data: [10,16]
        }]
    },
    options: {
        scales: {  
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

